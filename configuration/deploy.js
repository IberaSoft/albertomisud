/* eslint-disable no-console */
const FtpDeploy = require('ftp-deploy');
const environment = require('./environment');

const ftpDeploy = new FtpDeploy();


const config = {
  user: 'albertj',
  // Password optional, prompted if none given
  password: 'uvEftnmNJyZ7',
  host: 'ftp.cluster030.hosting.ovh.net',
  port: 21,
  // eslint-disable-next-line no-path-concat
  localRoot: environment.paths.output,
  remoteRoot: '/htdocs/',
  include: ['**/*'], // this would upload everything except dot files
  // e.g. exclude sourcemaps, and ALL files in node_modules (including dot files)
  // exclude: [
  //   'dist/**/*.map',
  //   'node_modules/**',
  //   'node_modules/**/.*',
  //   '.git/**',
  // ],
  // delete ALL existing files at destination before uploading, if true
  deleteRemote: true,
  // Passive mode is forced (EPSV command is not sent)
  forcePasv: true,
  // use sftp or ftp
  sftp: false,
};

ftpDeploy.on('uploading', function (data) {
  console.log(data.totalFilesCount); // total file count being transferred
  console.log(data.transferredFileCount); // number of files transferred
  console.log(data.filename); // partial path with filename being uploaded
});
ftpDeploy.on('uploaded', function (data) {
  console.log(data); // same data as uploading event
});
ftpDeploy.on('log', function (data) {
  console.log(data); // same data as uploading event
});
ftpDeploy.on('upload-error', function (data) {
  console.log(data.err); // data will also include filename, relativePath, and other goodies
});

ftpDeploy
  .deploy(config)
  .then((res) => console.log('finished:', res))
  .catch((err) => console.log(err));
