# Alberto Misud Website

# Development
## Build Assets

### One time build assets for development

```sh
$ npm run bootstrap
```

### Build assets and enable source files watcher

```sh
$ npm run watch
```

### Start a development server - reloading automatically after each file change.

```sh
$ npm run dev
```

### Build Assets without optimization files.

```sh
$ npm run build:dev
```

# Production
## Build Assets

Optimize assets for production by:

```sh
$ npm run build:prd
```

# Run Code Style Linters
## SASS

```sh
$ npm run lint:sass
```
## JavaScript

```sh
$ npm run lint:js
```

# Additional Tools
## Run Assets Bundle Analyzer

```sh
$ npm run stats
```

> This will open the visualisaion on the default configuraiton URL `localhost:8888`, you can change this URL or port following the [package](https://github.com/webpack-contrib/webpack-bundle-analyzer#options-for-cli) documentation.

## Continuous Integration
This app contains a development tool to deploy all files from `dist` folder through FTP.

```sh
$ npm run deploy
```
